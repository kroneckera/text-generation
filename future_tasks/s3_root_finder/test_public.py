import pytest
import math

from .root_finder import root_finder

ABSOLUTE_ERROR = 1e-9


def test_cube():
    def func(x):
        return x ** 3

    assert root_finder(func, -1, 1, accuracy=ABSOLUTE_ERROR) == pytest.approx(0, abs=ABSOLUTE_ERROR)


def test_exponent():
    def func(x):
        return math.exp(x) - 1234

    assert root_finder(func, -100, 100, accuracy=ABSOLUTE_ERROR) == pytest.approx(math.log(1234), abs=ABSOLUTE_ERROR)


def test_polynom_frac():
    def func(x):
        return (2 * x ** 7 - 3.4 * x ** 5 + 7845) / (3 * x ** 2 + 78) - 64

    result = root_finder(func, -1000, 1000, accuracy=ABSOLUTE_ERROR)
    assert result == pytest.approx(-2.6720836504496237, abs=ABSOLUTE_ERROR)


def test_multiroot():
    def func(x):
        return math.sin(x / 10)

    def get_all_roots_in_interval(left, right):
        # 10 * pi * n, n - integer
        least_root_n = int(math.ceil(left / 10. / math.pi))
        greatest_root_n = int(math.floor(right/ 10 / math.pi))

        if least_root_n <= greatest_root_n:
            return [10 * math.pi * n for n in range(least_root_n, greatest_root_n + 1)]
        else:
            return []

    left = 90
    right = 1000
    result = root_finder(func, left, right, accuracy=ABSOLUTE_ERROR)
    assert any([result == pytest.approx(root, abs=ABSOLUTE_ERROR)
                for root in get_all_roots_in_interval(left, right)])
