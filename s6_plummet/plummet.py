import pandas as pd


def find_toughest_plummet(
    coins: pd.DataFrame, symbol: str, start_date: str, end_date: str
) -> dict:
    """
    :param coins: coins price (in USD) and names for each date
    :param symbol: name of the traded coin
    :param start_date: begining of the date range
    :param end_date: end of the date range
    :return: date and value of maximal plummet
    """
    pass
