import pandas as pd


def compute_roi(
    coins: pd.DataFrame, investments: dict, start_date: str, end_date: str
) -> float:
    """
    :param coins: coins price (in USD) and names for each date
    :param investments: mapping from coin names to investments (in USD)
    :param start_date: buy date
    :param end_date: sell date
    :return: return of investments (ROI)
    """
    pass
