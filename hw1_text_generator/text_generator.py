from .load_save import save_model, load_model

from typing import Counter as TCounter
from typing import Dict, Tuple, Iterable, Sequence, List
from collections import defaultdict, Counter
import pathlib
import random
import os
import copy

import nltk
from nltk.tokenize import TweetTokenizer


nltk.download('punkt')


State = Tuple[str, ...]
MarkovChain = Dict[State, Dict[str, int]]
TokenizedSentence = Sequence[str]
Data = Iterable[TokenizedSentence]


def build_token_frequencies(data: Data) -> TCounter:
    """Initializes token_frequencies attribute with the counter
    of tokens in the data.

    Parameters
    ----------
    data
        The prepared data as a sequence of tokenized sentences.

    Returns
    -------
    Counter
        The counter of token frequencies in the data.
    """
    frequencies = Counter()
    for sentence in data:
        for token in sentence:
            frequencies[token] += 1
    return frequencies


class TextGeneratorModel:
    def __init__(self,
                 ngram_size: int,
                 smoothing: bool,
                 alpha: float = 0.4,
                 bos_token: str = '<BOS>',
                 eos_token: str = '<EOS>',
                 unk_token: str = '<UNK>'):
        """Initializes the TextGeneratorModel instance.

        Parameters
        ----------
        ngram_size
            The size of N-gram model.
        smoothing
            If the model uses smoothing. If not, the model skips edges
            from markov chain if absorbing state is not reachable
            from them. Otherwise, stupid backoff is used.
        alpha
            The parameter in the stupid backoff. Default is 0.4.
        bos_token
            The string representaton of the begin of sentence token.
            Default is '<BOS>'.
        eos_token
            The string representaton of the end of sentence token.
            Default is '<EOS>'.
        unk_token
            The string representaton of the unknown token.
            Default is '<UNK>'.

        """
        self._bos_token = bos_token
        self._eos_token = eos_token
        self._unk_token = unk_token
        self._ngram_size = ngram_size
        self._markov_chain = None
        self._token_frequencies = None
        self._smoothing = smoothing
        self.alpha = alpha

    @property
    def bos_token(self):
        """str: The string representaton of the begin of sentence token."""
        return self._bos_token

    @property
    def eos_token(self):
        """str: The string representaton of the end of sentence token."""
        return self._eos_token

    @property
    def unk_token(self):
        """str: The string representaton of the unknown token."""
        return self._unk_token

    @property
    def ngram_size(self):
        """int: The ngram size of the model."""
        return self._ngram_size

    @property
    def token_frequencies(self):
        """Counter: Token frequencies in the data."""
        return self._token_frequencies

    @property
    def smoothing(self):
        """str: Smoothing type of the model."""
        return self._smoothing

    @property
    def alpha(self):
        """float: The parameter of studif backoff."""
        return self._alpha

    @alpha.setter
    def alpha(self, value: float):
        """Raises
        ------
        ValueError
            If the value is not between 0 and 1.

        """
        if not 0 <= value <= 1:
            raise ValueError(
                f'An alpha parameter must be between 0 and 1, '
                f'but {value} given'
            )
        self._alpha = value

    def _build_markov_chain(self, data: Data, ngram_size: int,
                            reverse: bool = False) -> MarkovChain:
        """Generates markov chain from prepared data.
        It ignores states with unknown tokens.

        Parameters
        ----------
        data
            The prepared data as a sequence of tokenized sentences.
        ngram_size
            The ngram_size for current markov chain.
        reverse
            If True, generates the reversed markov chain: for
            each state it generates statistic for tokens before
            this state. Otherwise, generates usual markov chain.

        Returns
        -------
        MarkovChain
            The markov chain generates from given data.

        """
        markov_chain = defaultdict(Counter)
        prefix = [self.bos_token] * (ngram_size - 1)
        suffix = [self.eos_token] * (ngram_size - 1)
        for sentence in data:
            wrapped_sentence = prefix + sentence + suffix
            for i in range(len(wrapped_sentence) - ngram_size + 1):
                part = wrapped_sentence[i:i + ngram_size]
                if reverse:
                    current_state = tuple(part[1:])
                    next_token = part[0]
                else:
                    current_state = tuple(part[:-1])
                    next_token = part[-1]
                if self.unk_token not in part:
                    markov_chain[current_state][next_token] += 1
        return markov_chain

    def _build_absorbing_markov_chain(self, data: Data) -> MarkovChain:
        """Generates absorbing markov chain from prepared data. It ignores
        states with unknown tokens and removes edges from which
        the final state (<EOS> * (ngram_size - 1)) is not reachable.

        Parameters
        ----------
        data
            The prepared data as a sequence of tokenized sentences.

        Returns
        -------
        MarkovChain
            The absorbing markov chain generates from given data.

        """
        reversed_markov_chain = self._build_markov_chain(
            data, self.ngram_size, reverse=True
        )
        markov_chain = defaultdict(Counter)

        absorbing_state = tuple([self.eos_token] * (self.ngram_size - 1))
        dfs_stack = [
            (absorbing_state, iter(reversed_markov_chain[absorbing_state]))
        ]
        while dfs_stack:
            next_state, state_iter = dfs_stack[-1]
            try:
                prev_token = next(state_iter)
            except StopIteration:
                dfs_stack.pop()
                continue
            current_state = tuple([prev_token] + list(next_state)[:-1])
            next_token = next_state[-1]
            next_token_prob = reversed_markov_chain[next_state][prev_token]
            if (current_state not in markov_chain and
                    current_state in reversed_markov_chain):
                markov_chain[current_state][next_token] = next_token_prob
                dfs_stack.append((
                    current_state, iter(reversed_markov_chain[current_state])
                ))
            else:
                markov_chain[current_state][next_token] = next_token_prob
        return markov_chain

    def init_from_data(self, data: Data):
        """Initializes model from given sequence of tokenized sequence.

        Parameters
        ----------
        data
            The prepared data as a sequence of tokenized sentences.

        """
        if self.smoothing:
            self._markov_chains = {}
            for i in range(2, self.ngram_size + 1):
                self._markov_chains[i] = self._build_markov_chain(
                    data, ngram_size=i
                )
            self._token_frequencies = build_token_frequencies(data)
            del self._token_frequencies[self.unk_token]
        else:
            self._markov_chain = self._build_absorbing_markov_chain(data)

    def generate_next_token(self, state: State) -> str:
        """Generates next token for the given state.

        Parameters
        ----------
        state
            The last (ngram - 1) tokens of generated text.

        Returns
        -------
        str
            The next token.

        """
        if self.smoothing:
            weights = copy.deepcopy(self._token_frequencies)
            weights_sum = sum(weights.values())
            for token in weights:
                weights[token] /= weights_sum
            for i in range(2, self.ngram_size + 1):
                suffix_state = state[self.ngram_size - i:]
                if suffix_state in self._markov_chains[i]:
                    state_sum = sum(
                        self._markov_chains[i][suffix_state].values()
                    )
                    for token in self._markov_chains[i][suffix_state]:
                        weights[token] = (
                            self._markov_chains[i][suffix_state][token] /
                            self._alpha ** (i - 1) / state_sum
                        )
            return random.choices(
                list(weights), weights.values()
            )[0]

            chain_prob = self.alpha
            candidates_prob = {}
            for i in range(self.ngram_size, 1, -1):
                suffix_state = state[self.ngram_size - i:]
                if suffix_state in self._markov_chains[i]:
                    weights = self._markov_chains[i][suffix_state]
                    token = random.choices(
                        list(weights), weights.values()
                    )[0]
                    candidates_prob[token] = chain_prob
                    chain_prob *= self.alpha
            token = random.choices(
                list(self._token_frequencies),
                self._token_frequencies.values()
            )[0]
            candidates_prob[token] = chain_prob
            return random.choices(
                list(candidates_prob), candidates_prob.values()
            )[0]
        else:
            weights = self._markov_chain[state]
        return random.choices(
            list(weights), weights.values()
        )[0]

    def __eq__(self, other):
        """Compares to instances of TextGeneratorModel"""
        if not (self.smoothing == other.smoothing and
                self.bos_token == other.bos_token and
                self.eos_token == other.eos_token and
                self.unk_token == other.unk_token and
                self.ngram_size == other.ngram_size):
            return False
        if self.smoothing:
            return (
                self._markov_chains == other._markov_chains and
                self._alpha == other._alpha and
                self._token_frequencies == other._token_frequencies
            )
        else:
            return self._markov_chain == other._markov_chain


class TextGenerator:
    def __init__(self):
        self._unk_token = '<UNK>'
        self._skipped_punctuation = '!"#$%&\'()*+-/:;<=>?@[\\]^_`{|}~«»'

    @property
    def unk_token(self):
        """str: The string representaton of the unknown token."""
        return self._unk_token

    def _get_tokenized_sentences(self,
                                 data_path: pathlib.Path,
                                 extension: str = '.txt') -> Data:
        data_files = []
        if os.path.isdir(data_path):
            for root, dirnames, filenames in os.walk(data_path):
                for filename in filenames:
                    file_extension = os.path.splitext(filename)[1]
                    if file_extension == extension:
                        data_files.append(os.path.join(root, filename))
        else:
            data_files.append(data_path)

        tokenizer = TweetTokenizer()
        tokenized_sentences = []
        for filename in data_files:
            with open(filename, 'r', encoding='utf8') as input_file:
                current_sentences = []
                for line in input_file:
                    if line.strip():
                        current_sentences += nltk.sent_tokenize(line)
            tokenized_sentences += list(map(
                lambda sentence: list(filter(
                    lambda token: token not in self._skipped_punctuation,
                    tokenizer.tokenize(sentence.lower()),
                )),
                current_sentences
            ))
        return tokenized_sentences

    def _replace_rare_tokens(self,
                             tokenized_sentences: Data,
                             low_frequency_boundary: int = 10) -> Data:
        """Replace inplace tokens in 'tokenized_sentences' with
        frequency less than 'low_frequency_boundary' with
        self.unk_token.

        Parameters
        ----------
        tokenized_sentences
            The tokenized data.
        low_frequency_boundary
            The lower bound of non replaced token frequency.

        """

        token_count = build_token_frequencies(tokenized_sentences)

        for sentence in tokenized_sentences:
            for num, token in enumerate(sentence):
                if token_count[token] < low_frequency_boundary:
                    sentence[num] = self.unk_token

    def prepare_data(self,
                     data_path: pathlib.Path,
                     low_frequency_boundary: int = 10,
                     extension: str = '.txt') -> Data:
        """This method extract text from given data files, split
        it onto sequences, tokenize each sequence, remove some
        punctuation and replace low frequency tokens
        with unknown token.

        Parameters
        ----------
        data_path
            The path of the data. It could be a directory or a single
            file. In case of directory, data is accumulated from all
            files of the directory tree with 'extension' extension.
            Otherwise, single file is used regardless of
            file extension.
        low_frequency_boundaty
            The boundary for low frequency tokens: token is replaced
            with unknown token if its frequency in the data is less
            than 'low_frequency_boundaty'.
        extension
            If data_path is a directory then only files with this
            extension is used for data generation.

        Returns
        -------
        Data
            List of tokenized sentences.

        """
        tokenized_sentences = self._get_tokenized_sentences(
            data_path, extension
        )
        self._replace_rare_tokens(
            tokenized_sentences, low_frequency_boundary
        )
        return tokenized_sentences

    def build_model(self,
                    data: Data,
                    ngram_size: int = 2,
                    save_path=None,
                    smoothing: bool = True,
                    smoothing_alpha: float = 0.4) -> TextGeneratorModel:
        """
        Builds a model from the sequence of tokenized sentences.

        Parameters
        ----------
        data
            The prepared data as a sequence of tokenized sentences.
                ngram_size
        ngram_size
            The size of N-gram model.
        save_path
            If it is not None then saves model to this path.
        smoothing
            If the model uses smoothing. If not, the model skips edges
            from markov chain if absorbing state is not reachable
            from them. Otherwise, stupid backoff is used.
        smoothing_alpha
            The parameter in the stupid backoff.

        Returns
        -------
        TextGeneratorModel
            The builded model.

        """
        model = TextGeneratorModel(
            ngram_size=ngram_size,
            unk_token=self.unk_token,
            smoothing=smoothing,
            alpha=smoothing_alpha
        )
        model.init_from_data(data)
        if save_path is not None:
            save_model(save_path, model)
        return model

    def generate(self,
                 length: int,
                 model=None,
                 saved_model_path=None) -> List[str]:
        """
        Generates the list of tokens of given length, using
        given model. BOS and EOS token are skipped, token in
        the beginning of sentences are capitalized.

        Parameters
        ----------
        length
            The length of the generated list of tokens.
        model
            The N-gram model.
        saved_model_path
            Path to N-gram model. If 'model' parameter is None then
            loads model from this path.

        Returns
        -------
        List[str]
            Generated text.

        """
        if model is None:
            model = load_model(saved_model_path)
        text = []
        while len(text) < length:
            sentence = [model.bos_token] * (model.ngram_size - 1)
            while (sentence[-1] not in [model.eos_token, '.', '!', '?'] and
                   len(sentence) < length + model.ngram_size * 2):
                state = tuple(sentence[-model.ngram_size + 1:])
                sentence.append(model.generate_next_token(state))
            if sentence[-1] == model.eos_token:
                sentence.pop()
            if sentence[-1] not in ['.', '!', '?']:
                sentence.append('.')
            sentence = sentence[model.ngram_size - 1:]
            for i in range(len(sentence)):
                if sentence[i] != sentence[i].capitalize():
                    sentence[i] = sentence[i].capitalize()
                    break
            text += sentence
        text = text[:length]
        return text
