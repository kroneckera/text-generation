import pandas as pd


def luck_factor(
    coins: pd.DataFrame, symbol: str, start_date: str, end_date: str
) -> float:
    """
    :param coins: coins low, high prices (in USD) and names for each date
    :param symbol: name of the traded coin
    :param start_date: first day of trading
    :param end_date: last day of trading
    :return: investments growth factor
    """
    pass
