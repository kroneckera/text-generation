from .matrix import traverse_matrix


def test_trivial():
    assert traverse_matrix([[1]]) == [1]


def test_small():
    assert traverse_matrix(
        [
            [1, 2],
            [3, 4],
        ]
    ) == [1, 2, 4, 3]

    assert traverse_matrix(
        [
            [1, 2, 3],
            [4, 5, 6],
            [7, 8, 9],
        ]
    ) == [1, 2, 3, 6, 9, 8, 7, 4, 5]


def test_etalon():
    matrix = [
        [1,  2,  3,  4],
        [5,  6,  7,  8],
        [9, 10, 11, 12],
        [13, 14, 15, 16],
    ]
    assert traverse_matrix(matrix) == [1, 2, 3, 4, 8, 12, 16, 15, 14, 13, 9, 5, 6, 7, 11, 10]


def test_big_zero_matrix():
    size = 20
    matrix = [[0 for j in range(size)] for i in range(size)]
    assert traverse_matrix(matrix) == [0] * (size**2)


def test_big_one_matrix():
    size = 32
    matrix = [[1 for j in range(size)] for i in range(size)]
    assert traverse_matrix(matrix) == [1] * (size**2)
