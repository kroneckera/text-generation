import pickle


def save_model(path, model):
    with open(path, 'wb') as output:
        pickle.dump(model, output)


def load_model(path):
    with open(path, 'rb') as input_file:
        model = pickle.load(input_file)
    return model
