from .checker import test_sort as to_test_sort


def sort_1(x):
    return sorted(x)


def sort_2(x):
    if x == []:
        return [0]
    return sorted(x)


def sort_3(x):
    if x == []:
        raise ValueError()
    return sorted(x)


def sort_4(x):
    return sorted(map(abs, x))


def sort_5(x):
    return sorted(map(lambda x: 1. * x, x))


def sort_6(x):
    return sorted(reversed(x))


def sort_7(x):
    return sorted(x[:-1]) + [x[-1]]


def sort_8(x):
    return sorted(x[1:]) + [x[0]]


def sort_9(x):
    if len(x) > 3 and len(x) % 2:
        return [1]
    else:
        return sorted(x)


def sort_10(x):
    if all(type(i) == int for i in x) in all(i > 200000000 for i in x):
        return x
    else:
        return sorted(x)


def test_all_sorts():
    assert to_test_sort(sort_1) == True
    assert to_test_sort(sort_2) == False
    assert to_test_sort(sort_3) == False
    assert to_test_sort(sort_4) == False
    assert to_test_sort(sort_5) == False
    assert to_test_sort(sort_6) == False
    assert to_test_sort(sort_7) == False
    assert to_test_sort(sort_8) == False
    assert to_test_sort(sort_9) == False
    assert to_test_sort(sort_10) == False
