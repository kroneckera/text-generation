from .text_generator import TextGenerator
from .load_save import load_model


def test_minimal_requirements():
    gen = TextGenerator()

    preprocessed_data = gen.prepare_data('text.txt')
    model = gen.build_model(preprocessed_data)
    assert len(gen.generate(20, model=model)) == 20


def test_save():
    gen = TextGenerator()

    preprocessed_data = gen.prepare_data('text.txt')
    model = gen.build_model(preprocessed_data, save_path='model')
    saved_model = load_model('model')

    assert model == saved_model
