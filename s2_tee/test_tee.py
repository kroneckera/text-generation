import subprocess
import sys
import string
import random
import os


def generate_string(size=6, chars=string.ascii_letters + string.digits):
    return bytes(''.join(random.choice(chars) for _ in range(size)), encoding='utf-8')


def test_communication():
    tee = subprocess.Popen(
        [sys.executable, 'tee.py'],
        cwd='.',
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        stdin=subprocess.PIPE
    )

    message = bytes('Hello', encoding='utf-8') + generate_string(1000)

    my_stdout, my_stderr = tee.communicate(input=message)

    assert (my_stdout == message) or (my_stdout == message + bytes(os.linesep, encoding='utf-8'))
    assert (my_stderr == message) or (my_stderr == message + bytes(os.linesep, encoding='utf-8'))
