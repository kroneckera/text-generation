## Готовим виртуалку Яндекс.Облака к запуску flask (не production-ready вариант)

### Что описывается в данном документе
 В тексте ниже будет показано как настроить виртуальную машинку ниже так, чтобы на ней можно было запустить приложение flask, до которого можно достучаться из интернетов. В процессе посмотрим на некоторые bash команды. Туториал показывает с какими сложностями можно столкнуться и как их решать.
 
 **Disclaimer**: Представленный способ рабочий, но игнорирует ряд важных для продакшен систем требований. Например, не производится никаких действий для усиления безопасности.

### Предпосылки
* Зарегистрировались в Яндекс.Облако
* Cоздали виртуальную машинку на основе ubuntu18.04
* Можете подключиться к машинке по ssh

### На сервере
После подключения через ssh вы попадаете в некое окружение. Там можно писать разные команды на bash (советую ознакомиться с туториалами [Learn Enough Command Line to Be Dangerous](https://www.learnenough.com/command-line-tutorial/basics))

Давайте создадим папку под проект и перейдем в нее:
```
mkdir demo_app
cd demo_app
```

Пробуем запустить python:
```bash
python
```
Получаем следующий результат:
```
Command 'python' not found, but can be installed with:

apt install python3
apt install python
apt install python-minimal

Ask your administrator to install one of them.

You also have python3 installed, you can run 'python3' instead.
```

Ок, значит у нас есть в наличии python3! Пробуем запустить:
```bash
python3
```
Получаем работающий интерпретатор, ожидающий ввода команд:
```
Python 3.6.7 (default, Oct 22 2018, 11:32:17)
[GCC 8.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
>>>
```

#### Создаем виртуальное окружение
Выходим из REPL (нажимаем `Ctrl+d`) и задаемся целью создать виртуальное окружение. 
С использованием третьего питона это можно сделать с помощью модуля `venv`:
```bash
python3 -m venv .venv
``` 
Результат огорачает:
```
The virtual environment was not created successfully because ensurepip is not
available.  On Debian/Ubuntu systems, you need to install the python3-venv
package using the following command.

    apt-get install python3-venv

You may need to use sudo with that command.  After installing the python3-venv
package, recreate your virtual environment.

Failing command: ['/home/kipnell/demo_app/.venv/bin/python3', '-Im', 'ensurepip', '--upgrade', '--default-pip']
```

Начинаем упражнение по установке пакетов в систему посредством [apt](https://ru.wikipedia.org/wiki/Advanced_Packaging_Tool). Пробуем команду, предложенную в ошибке:
```bash
apt-get install python3-venv
```

На что система нам намекает, что нам не хватает прав для данного действия.
```
E: Could not open lock file /var/lib/dpkg/lock - open (13: Permission denied)
E: Unable to lock the administration directory (/var/lib/dpkg/), are you root?
```

Чтож, покажем ей кто здесь главный:
```bash
sudo apt-get install python3-venv
```

Но система опять не смогла:
```
Reading package lists... Done
Building dependency tree
Reading state information... Done
Package python3-venv is not available, but is referred to by another package.
This may mean that the package is missing, has been obsoleted, or
is only available from another source

E: Package 'python3-venv' has no installation candidate
```

Ошибка говорит, что есть некий пакет python3-venv, но откуда его взять система не знает.
Прежде чем добавлять новые репозитории в списки пакетного менеджера, давайте попробобуем обновить
списки пакетов из уже подключенных репозиториев:
```bash
sudo apt-get update
```

После этого можно обновить (но не будем этого делать) пакеты с помощью команды:
```bash
sudo apt-get upgrade
```

Попробуем установить пакет с venv еще раз:
```bash
sudo apt-get install python3-venv
```

Все получилось! Пробуем создать виртуальное окружение:
```bash
python3 -m venv .venv
``` 

Никаких ошибок не обнаружено. Значит виртуальное окружение собралось и доступно в папке `.venv`.
Смотрим файловую структуру текущей папки:
```bash
ls
```

... и ничего. Все потому, что папка `.venv` "скрытая", т.к. ее имя начинается с точки. 
Обычно в скрытых файлах и директориях хранится конфигурационная информация для каких-либо программ.
Чтобы увидеть скрытые файлы нужно добавить ключ `-a` (сокращение для `--all`) к команде `ls`.
```bash
ls -a
la  # alias для 'ls -a'
```

Действительно папка `.venv` на месте. Какие ключи еще есть у команды `ls`? Ответ можно узнать
вопользовавшить командой `man` (от manual). 
```bash
man ls  # выход по клавише q
```

На большинство встроенных команд есть страничка в `man`. Еще help можно попробовать посмотреть добавив `-h` (`--help`) к команде. Например 
```bash 
python3 -h
```

Ок, как смотреть документацию к командам разобрались. Вернемся к виртуальному окружению.
```bash
source .venv/bin/activate
```

Активировали окружение. Теперь по команде `python3` (в рамках этого терминала) будет запускаться интерпретатор из виртуального окружения. Проверить это можно с помощью команды `which`. Она находит тот исполняемый файл, который запускается при вводе команды.
```bash
which python3  # /home/kipnell/demo_app/.venv/bin/python3
```

Выйти из виртуального окружения можно командой `deactivate` (сравните вывод `which python3` с активированным окружением и без него).


### Работаем с приложением
Активируем окружение (если из него выходили) снова и установим в него `flask`:
`pip install flask`

На своем компьютере разрабатываем сверхприложение (у меня файл будет называтьтся `app.py`):
```python
from flask import Flask
app = Flask(__name__)

@app.route('/')
def hello_world():
    return 'Hello, World!'
```

#### Копируем файл с помощью scp
В отдельном терминале копируем этот файл на удаленную машину командой `scp` (работает для linux и macos пользователей, в windows может отличаться):
```bash
scp -i ~/.ssh/mydemo_rsa ~/PycharmProjects/demo/app.py kipnell@100.100.100.100:~/demo_app/app.py
```
 Конечно, у вас будут другие пути до файлов, логины, ip-адреса. Плюс я дополнительно указал ключом `-i` путь до своего приватного ключа для коннекта. Если приходится часто подключаться к удаленной машине (и вы работаете на ubnutu или macos), то можно опции подключения сохранить в конфигурационный файл
`~/.ssh/config`. Например для этого подключения в этот файл можно добавить запись
```
Host hse-demo
    HostName 100.100.100.100
    User kipnell
    IdentityFile ~/.ssh/mydemo_rsa
```
и теперь можно сократить команду выше до:
```bash
scp ~/PycharmProjects/demo/app.py hse-demo:~/demo_app/app.py
```
а подключаться к удаленной машинке командой:
```bash
ssh hse-demo
```

#### Запуск приложения
Возвращаемся в терминал с удаленной машинкой. С помощью `ls` убеждаемся, что файл на месте. Можно его посмотреть  с помощью каких-либо инструментов (`cat`, `nano`, `less`, `vim`). Запускаем приложение:
```bash
FLASK_APP=app.py flask run --host 0.0.0.0
```

Теперь можно в браузере по адресу `http://100.100.100.100:5000` и засвидетельствовать рождение `Hello, world!` (вместо `100.100.100.100` должен быть публичный IP вашей машины)

### Установка других пакетов
Доустанавливаем `tmux`, `npm`, `localtunnel` и идем разбираться что это за дикие звери.
```bash
sudo apt install tmux npm
sudo npm install -g localtunnel
```