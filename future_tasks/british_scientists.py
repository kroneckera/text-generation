import re
import random


def shuffle_letters(matched_groups, count):
    if count <= 1:
        return matched_groups.group(0)

    first, *inner_letters, last = matched_groups.group(1), matched_groups.group(2), matched_groups.group(3)
    count = min(len(inner_letters), count)

    shuffled = random.shuffle()
    return first + shuffled + last


def shuffle_letters_in_words(text, count):

    def shuffler(matched):
        shuffle_letters(matched, count)

    return re.sub(
        r'(\w)(\w+)(\w)',
        shuffler,
        text
    )
